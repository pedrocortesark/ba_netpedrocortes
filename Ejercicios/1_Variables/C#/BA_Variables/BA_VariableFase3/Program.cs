﻿using System;

namespace BA_VariableFase3
{
    class Program
    {
        public const int leapRepeat = 4;

        static void Main(string[] args)
        {
            Console.Write("Introduce tu año de nacimiento, bicho!\n\r");

            int birthYear;
            string birthYearStr = Console.ReadLine();
            int leapYear = 1948;
            bool isLeapYear = false;
            string yearYes = "Mi año de nacimiento es año bisiesto";
            string yearNo = "Mi año de nacimiento no es año bisiesto";

            if (Int32.TryParse(birthYearStr, out birthYear))
            {

                Console.Write("La lista de año desde 1948 es la siguiente:\n\r");

                while (birthYear >= leapYear)
                {
                    Console.WriteLine(leapYear);
                    if (leapYear == birthYear)
                    {
                        isLeapYear = true;
                        break;
                    }
                    else
                        leapYear += leapRepeat;
                }

                Console.WriteLine();
                if (isLeapYear == true)
                    Console.WriteLine(yearYes);
                else
                    Console.WriteLine(yearNo);
            }
            else
                Console.WriteLine("Introduce un número válido!");
         
        }
    }
}
