﻿using System;

namespace BA_VariablesFase1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hola! Escribe tu nombre por favor!");
            string name = Console.ReadLine();
            Console.WriteLine("Escribe tu primer apellido por favor!");
            string lastname1 = Console.ReadLine();
            Console.WriteLine("Escribe tu segundo apellido por favor!");
            string lastname2 = Console.ReadLine();

            Console.WriteLine("Hola! Escribe tu día de nacimiento!");
            string day = Console.ReadLine();
            Console.WriteLine("Escribe tu mes de nacimiento!");
            string month = Console.ReadLine();
            Console.WriteLine("Escribe tu año de nacimiento");
            string year = Console.ReadLine();

            var bar = "/";

            string student = lastname1 + " " + lastname2 + ", " + name;
            string date = String.Concat(day, bar, month, bar, year);

            Console.WriteLine("\n\rPues estos son los datos que has introducido!");
            Console.WriteLine(student);
            Console.WriteLine(date);
        }
    }
}
