﻿using System;

namespace BA_VariablesFase2
{
    class Program
    {
        public const int leapYear = 1948;
        public const int leapRepeat = 4;

        static void Main(string[] args)
        {
            Console.Write("Introduce tu año de nacimiento, bicho!\n\r");
            string birthYearStr = Console.ReadLine();
            int birthYear;

            if (Int32.TryParse(birthYearStr, out birthYear))
            {
                var moveNum = (birthYear - leapYear) / leapRepeat;
                Console.WriteLine("Desde el año 1948, hubo " + moveNum + " años bisiestos.");
            }

            else
                Console.WriteLine("El número introducido es incorrecto!");
            

        }
    }
}
