﻿using System;
using System.Collections.Generic;

namespace BA_LletresFase2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Bon dia! Introduce tu nombre, que vamos a jugar un rato...");
            string name = Console.ReadLine();
            List<char> listCharactersNom = new List<char>();

            for (int i = 0; i < name.Length; i++)
            {
                if (Char.IsDigit(name[i]) || Char.IsWhiteSpace(name[i]))
                {
                    Console.WriteLine("Els noms de persones no contenen números ni espacios! mequetrefe!");
                }
                else
                {
                    char lletreUp = Char.ToUpper(name[i]);
                    listCharactersNom.Add(lletreUp);
                    string lletreStr = lletreUp.ToString().Normalize();

                    if (lletreStr == "A" || lletreStr == "E" || lletreStr == "I" || lletreStr == "O" || lletreStr == "U")
                        Console.WriteLine("This is a vowel!!");
                    else
                        Console.WriteLine("This is a consonant!!");

                }
            }
        }
    }
}
