﻿using System;

namespace BA_Ciutats
{
    class Program
    {
        static void Main(string[] args)
        {
            string city1 = "", city2 = "", city3 = "", city4 = "", city5 = "", city6 = "";

            Console.WriteLine("Bienvenido al mundo de las ciudades!");
            Console.WriteLine("Por favor, introduce el nombre de 6 ciudades");

            for (int i = 1; i <= 6; i++)
            {
                Console.WriteLine($"Introduce el nombre de la ciudad {i}");

                if (i == 1)
                    city1 = Console.ReadLine();
                else if (i == 2)
                    city2 = Console.ReadLine();
                else if (i == 3)
                    city3 = Console.ReadLine();
                else if (i == 4)
                    city4 = Console.ReadLine();
                else if (i == 5)
                    city5 = Console.ReadLine();
                else
                    city6 = Console.ReadLine();
            }

            Console.WriteLine($"\r\nLas ciudades que has introducido son las siguientes: {city1}, {city2}, {city3}, {city4}, {city5} y {city6}");
        }
    }
}
