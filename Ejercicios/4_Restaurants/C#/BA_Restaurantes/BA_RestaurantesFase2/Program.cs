﻿using System;
using System.Collections.Generic;

namespace BA_RestaurantesFase2
{
    class Program
    {
        static void Main(string[] args)
        {
            double totalPreu = 0.0;
            int numPlats = 5;
            var miPedido = new List<string>();

            int b500 = 500;
            int b200 = 200;
            int b100 = 100;
            int b50 = 50;
            int b20 = 20;
            int b10 = 10;
            int b5 = 5;

            string[] myPlats = new string[numPlats];
            double[] myPreus = new double[numPlats];

            for (var i = 0; i < numPlats; i++)
            {
                Console.WriteLine($"Escriba el nombre del plato número {i + 1}");
                string plat = Console.ReadLine();
                while (String.IsNullOrEmpty(plat))
                {
                    Console.WriteLine("El formato de entrada es incorrecto. Por favor, introduce el primer plato del menú");
                    plat = Console.ReadLine();
                }
                myPlats[i] = plat;

                Console.WriteLine($"Escriba el precio del plato número {i + 1}");
                string preuStr = Console.ReadLine();
                double preu;

                while (!Double.TryParse(preuStr, out preu))
                {
                    Console.WriteLine("El precio debe ser un número. Introduzca de nuevo el precio.");
                    preuStr = Console.ReadLine();
                }
                myPreus[i] = preu;
            }

            Console.WriteLine("\n\rBienvenido al mejor restaurante de esta galaxia!");
            Console.WriteLine("Estos son los platos del menú del día de hoy\n\r");
            Console.WriteLine("Plats\t\t\tPreus");
            Console.WriteLine("------\t\t\t------");

            for (var i = 0; i < numPlats; i++)
            {
                Console.WriteLine($"{myPlats[i]}\t\t\t{myPreus[i]}");
            }

            int pedirMas = 1;
            Console.WriteLine("\n\rUna vez vista la carta... ¿Qué es lo que quiere comer hoy?");

            while (pedirMas == 1)
            {
                Console.WriteLine("Indique uno de los platos del menú.");
                var pedido = Console.ReadLine();
                var isInMenu = false;

                while (isInMenu == false)
                {
                    for (var i = 0; i < numPlats; i++)
                    {
                        if (myPlats[i] == pedido)
                            isInMenu = true;
                    }

                    if (isInMenu == false)
                    {
                        Console.WriteLine("\n\rEl plato que ha solicitado no se encuentra en el menú de hoy.");
                        Console.WriteLine("Lea bien la carta y escriba el plato que desee probar, melón.");
                        pedido = Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine($"\n\rEl plato {pedido} se ha añadido a su pedido para hoy.");
                        miPedido.Add(pedido);
                        Console.WriteLine("\n\rLos platos solicitados hasta este momento son:");
                        foreach (var element in miPedido)
                            Console.WriteLine(element);
                    }
                }

                Console.WriteLine("\n\r¿Quiere pedir algo más de comer?");
                Console.WriteLine("Pulsa 1 para seguir pidiendo o pulsa 0 para solicitar ya la cuenta.");
                var opcionMas = Console.ReadLine();

                while (!ValidateVoleuMes(opcionMas))
                {
                    Console.WriteLine("Por favor, introduce 1 para seguir pidiendo o 0 para solicitar la cuenta.");
                    opcionMas = Console.ReadLine();
                }

                Int32.TryParse(opcionMas, out int resultado);
                pedirMas = resultado;

                if (pedirMas == 1)
                    continue;

            }

            Console.WriteLine("Pues esa ha sido la experiencia completa en nuestro restaurante! Esperamos verle pronto de vuelta!");
            Console.ReadLine();

        }

        static bool ValidateVoleuMes(string entrada)
        {
            if (Int32.TryParse(entrada, out int result))
            {
                if (result == 0 || result == 1)
                    return true;
                else
                    return false;
            }
            else
                return false;         
        }


    }
}
