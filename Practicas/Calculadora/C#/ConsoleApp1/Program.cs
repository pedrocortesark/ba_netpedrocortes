﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        //Voy a declarar el mecanismo de escape antes del programa
        //static char EscapeCharacter = 'R';
        static string EscapeWord = "READY";

        static void Main(string[] args)
        {
            Console.WriteLine("Hola! Bienvenid@ a tu clase");
            Console.WriteLine("Introduzca notas de los alumnos");

            var notasAlumnos = new List<double>();

            //definimos el carácter de escape
            var keepDoing = true;

            //vamos a generar la válvula de escap de nuestro while
            while (keepDoing)
            {
                //Vamos primero a leer por consola y a almacenarlo en una variable
                Console.WriteLine("La nota del alumno {0}", notasAlumnos.Count + 1);
                var notaText = Console.ReadLine();

                //Creamos el if para saber si escapamos del bucle
                if (notaText == EscapeWord)
                {
                    keepDoing = false;
                }
                else
                {
                    //Vamos a decidir qué es lo que queremos hacer con nuestro valor
                    if (double.TryParse(notaText.Replace(".", ","), out double nota))
                    {
                        notasAlumnos.Add(nota);
                    }
                    else
                    {
                        Console.WriteLine("Que clase de número has metido loco??");
                    }
                }
            }


            //Vamos a ver la nota más alta y la más baja
            double notaAlta = 0.0;
            bool hayNotaAlta = false;
            double notaBaja = 0.0;
            bool hayNotaBaja = false;

            foreach (var nota in notasAlumnos)
            {
                if (hayNotaAlta is false)
                {
                    notaAlta = nota;
                    hayNotaAlta = true;
                }
                else
                {
                    if (nota > notaAlta)
                    {
                        notaAlta = nota;
                    }
                }

                if (hayNotaBaja is false)
                {
                    notaBaja = nota;
                    hayNotaBaja = true;
                }
                else
                {
                    if (nota < notaBaja)
                    {
                        notaBaja = nota;
                    }
                }

            }


            

            //Vamos a ver la nota más baja

            

            //Vamos a calcula la media
            var suma = 0.0;

            for (var i = 0; i < notasAlumnos.Count; i++)
            {
                suma += notasAlumnos[i];
            }

            var average = suma / notasAlumnos.Count;
            Console.WriteLine("La nota media de todos los alumnos es: {0},", average);
            Console.WriteLine("La nota más alta es: {0}", notaAlta);
            Console.WriteLine("La nota más baja es: {0}", notaBaja);


            //Mantener abierta la consola hasta que yo teclee algo
            Console.ReadKey(true);

        }
    }
}
